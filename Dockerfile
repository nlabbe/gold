FROM node:9.2.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .

RUN npm install

CMD npm run start

EXPOSE 3000
