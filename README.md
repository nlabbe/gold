docker compose

```
sudo docker-compose up -d
```

docker logs

```
sudo docker-compose logs -f
```

inspect

```
docker container list
docker exec -ti $CONTAINER_ID /bin/bash
```


```
# Build Dockerfile and remove old "gold" images after success
docker build -t gold .
```

```
# Run docker file
docker run -it -p 3000:3000 --name gold -v "${PWD}:/usr/src/app" gold
```

```
# Run docker compose
docker-compose -f docker-compose.dev.yml up --build
```